package com.bca.controller;

import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import adapter.LogAdapter;
import adapter.CISAdapter;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {
	String ConnectionStatus = "true";
	return ConnectionStatus;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/{cis}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetCIS(@PathVariable("cis") String id, @RequestParam(value = "k1", defaultValue = "0") String k1Flag,
	    @RequestParam(value = "code", defaultValue = "12") String functionCode, @RequestParam(value = "serial", defaultValue = "") String serialNumber,
	    @RequestParam(value = "type", defaultValue = "cis") String type, @RequestParam(value = "wsid", defaultValue = "") String wsid,
	    @RequestParam(value = "menu", defaultValue = "") String menu, HttpServletResponse response) {

	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;

	model.mdlAPIResult mdlGetCISResult = new model.mdlAPIResult();
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();

	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = wsid;
	mdlLog.SerialNumber = serialNumber;
	mdlLog.ApiFunction = "getCIS";
	mdlLog.SystemFunction = "GetCIS";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";
	boolean isValid = true;
	Gson gson = new Gson();
	Object[] params = new Object[] { id, k1Flag, serialNumber, wsid };
	String jsonInTemplate = type.equalsIgnoreCase("cis") ? "CIS : {0}, K1Flag : {1}, SerialNumber : {2}, WSID : {3}" : "KTP : {0}, K1Flag : {1}, SerialNumber : {2}, WSID : {3}";
	String jsonIn = MessageFormat.format(jsonInTemplate, params);
	String jsonOut = "";

	try {
	    String cisCustomer = "";
	    if (!type.equalsIgnoreCase("cis")) {
		// if type is not cis, then assume that the cis is KTP ID
		model.mdlAPIResult mdlCISByKTPResult = new model.mdlAPIResult();
		mdlCISByKTPResult = CISAdapter.getCISByKTP(id, serialNumber, wsid);
		if (mdlCISByKTPResult == null || mdlCISByKTPResult.ErrorSchema.ErrorCode == null) {
		    mdlErrorSchema.ErrorCode = "05";
		    mdlMessage.Indonesian = "Gagal memanggil service getCISByKTP";
		    mdlMessage.English = mdlLog.ErrorMessage = "Service getCISByKTP call failed";
		    mdlErrorSchema.ErrorMessage = mdlMessage;
		    mdlGetCISResult.ErrorSchema = mdlErrorSchema;
		    LogAdapter.InsertLog(mdlLog);
		    jsonOut = gson.toJson(mdlGetCISResult);
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    return jsonOut;
		} else if (!mdlCISByKTPResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		    if (mdlCISByKTPResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-99-731")) {
			cisCustomer = "00000000000"; // Error ESB-99-731 - Nomor Customer tidak ditemukan, cin jadi default 00000000000
		    } else {
			mdlErrorSchema.ErrorCode = mdlCISByKTPResult.ErrorSchema.ErrorCode;
			mdlMessage.Indonesian = mdlCISByKTPResult.ErrorSchema.ErrorMessage.Indonesian;
			mdlMessage.English = mdlLog.ErrorMessage = mdlCISByKTPResult.ErrorSchema.ErrorMessage.English;
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlGetCISResult.ErrorSchema = mdlErrorSchema;
			LogAdapter.InsertLog(mdlLog);
			jsonOut = gson.toJson(mdlGetCISResult);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return jsonOut;
		    }

		} else {
		    model.mdlCISByKTP mdlCISByKTP = new model.mdlCISByKTP();
		    String OutputSchemaString = gson.toJson(mdlCISByKTPResult.OutputSchema);
		    mdlCISByKTP = gson.fromJson(OutputSchemaString, model.mdlCISByKTP.class);
		    List<model.mdlCIS> listCIS = mdlCISByKTP.CIS;
		    if (listCIS.size() == 0) {
			cisCustomer = "00000000000";
		    } else if (listCIS.size() == 1) {
			String firstCISNumber = listCIS.get(0).CISCustomerNumber;
			cisCustomer = firstCISNumber.equalsIgnoreCase("") || firstCISNumber == null ? "00000000000" : firstCISNumber;
		    } else {
			if (menu.equals("8")) {// khusus untuk pemrek, maka return error  
			    mdlErrorSchema.ErrorCode = "06";
			    mdlMessage.Indonesian = "Nomor CIS lebih dari satu";
			    mdlMessage.English = mdlLog.ErrorMessage = "CIS number are more than one";
			    mdlErrorSchema.ErrorMessage = mdlMessage;
			    mdlGetCISResult.ErrorSchema = mdlErrorSchema;
			    LogAdapter.InsertLog(mdlLog);
			    jsonOut = gson.toJson(mdlGetCISResult);
			    stopTime = System.currentTimeMillis();
			    elapsedTime = stopTime - startTime;
			    logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", mdlCISByKTPResult : "
				    + gson.toJson(mdlCISByKTPResult) + ", jsonOut:" + jsonOut);
			    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			    return jsonOut;
			} else {
			    for (model.mdlCIS cin : listCIS) {
				String CISNumber = cin.CISCustomerNumber;
				cisCustomer = CISNumber.equalsIgnoreCase("") || CISNumber == null ? "00000000000" : CISNumber;
			    }
			}

		    }
		}
	    } else {
		cisCustomer = id;
	    }

	    if (cisCustomer == null || cisCustomer.equals("")) {
		mdlErrorSchema.ErrorCode = "01";
		mdlMessage.Indonesian = "Nomor CIS tidak valid";
		mdlMessage.English = mdlLog.ErrorMessage = "CIS number is not valid";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetCISResult.ErrorSchema = mdlErrorSchema;
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlGetCISResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

	    } else if (cisCustomer.equals("00000000000")) {
		JsonObject mObj = new JsonObject();
		mObj.addProperty("cis_customer_number", "00000000000");
		mdlErrorSchema.ErrorCode = "00";
		mdlMessage.Indonesian = "Berhasil";
		mdlMessage.English = mdlLog.LogStatus = "Success";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetCISResult.ErrorSchema = mdlErrorSchema;
		mdlGetCISResult.OutputSchema = gson.fromJson(mObj, Object.class);
		jsonOut = gson.toJson(mdlGetCISResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.info("SUCCESS = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		return jsonOut;
	    }

	    model.mdlAPIResult mdlAPIResult = CISAdapter.GetCISData(cisCustomer, k1Flag, functionCode, serialNumber, wsid);

	    if (mdlAPIResult == null || mdlAPIResult.ErrorSchema == null) {
		mdlErrorSchema.ErrorCode = "02";
		mdlMessage.Indonesian = "Gagal memanggil service GetCISData";
		mdlMessage.English = mdlLog.ErrorMessage = "Service GetCISData call failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetCISResult.ErrorSchema = mdlErrorSchema;
		isValid = false;
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlErrorSchema.ErrorCode = mdlAPIResult.ErrorSchema.ErrorCode;
		mdlMessage.Indonesian = mdlAPIResult.ErrorSchema.ErrorMessage.Indonesian;
		mdlMessage.English = mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlGetCISResult.ErrorSchema = mdlErrorSchema;
		isValid = false;
	    }

	    if (!isValid) {
		LogAdapter.InsertLog(mdlLog);
		jsonOut = gson.toJson(mdlGetCISResult);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		return jsonOut;
	    }

	    LinkedTreeMap<String, JsonObject> outputSchemaMap = (LinkedTreeMap<String, JsonObject>) mdlAPIResult.OutputSchema;
	    JsonObject mObj = gson.toJsonTree(outputSchemaMap).getAsJsonObject();

	    String cisType = mObj.get("CISCustomerType").getAsString();
	    // check for cis type, individu or organization
	    if (cisType.equalsIgnoreCase("I")) {
		model.mdlCISIndividu mdlCISIndividu = new model.mdlCISIndividu();
		String jsonString = mObj.toString();
		// inject camel case json to model
		mdlCISIndividu = gson.fromJson(jsonString, model.mdlCISIndividu.class);
		// convert model to string with snake case properties
		String cisInString = gson.toJson(mdlCISIndividu);
		mObj = gson.fromJson(cisInString, JsonObject.class);
	    } else {
		// assume that the cis type is organization if not individu
		model.mdlCISOrganization mdlCISOrganization = new model.mdlCISOrganization();
		String jsonString = mObj.toString();
		// inject camel case json to model
		mdlCISOrganization = gson.fromJson(jsonString, model.mdlCISOrganization.class);
		String cisInString = gson.toJson(mdlCISOrganization);
		// convert model to string with snake case properties
		mObj = gson.fromJson(cisInString, JsonObject.class);
	    }
	    // check for k1 flag from request param
	    if (!k1Flag.equals("0")) {
		model.mdlAPIResult mdlK1FlagResult = new model.mdlAPIResult();
		mdlK1FlagResult = CISAdapter.GetK1Flag(cisCustomer, serialNumber, wsid);
		isValid = true;
		if (mdlK1FlagResult == null || mdlK1FlagResult.ErrorSchema == null) {
		    mdlErrorSchema.ErrorCode = "03";
		    mdlMessage.Indonesian = "Gagal memanggil service GetK1Flag";
		    mdlMessage.English = mdlLog.ErrorMessage = "Service GetK1Flag call failed";
		    mdlErrorSchema.ErrorMessage = mdlMessage;
		    mdlGetCISResult.ErrorSchema = mdlErrorSchema;
		    isValid = false;
		} else if (!mdlK1FlagResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		    mdlErrorSchema.ErrorCode = mdlK1FlagResult.ErrorSchema.ErrorCode;
		    mdlMessage.Indonesian = mdlK1FlagResult.ErrorSchema.ErrorMessage.Indonesian;
		    mdlMessage.English = mdlLog.ErrorMessage = mdlK1FlagResult.ErrorSchema.ErrorMessage.English;
		    mdlErrorSchema.ErrorMessage = mdlMessage;
		    mdlGetCISResult.ErrorSchema = mdlErrorSchema;
		    isValid = false;
		}

		if (!isValid) {
		    LogAdapter.InsertLog(mdlLog);
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    jsonOut = gson.toJson(mdlGetCISResult);
		    logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		    return jsonOut;
		}

		outputSchemaMap = (LinkedTreeMap<String, JsonObject>) mdlK1FlagResult.OutputSchema;
		JsonObject k1Object = gson.toJsonTree(outputSchemaMap).getAsJsonObject();
		JsonElement rowArray = k1Object.get("row");
		Type listType = new TypeToken<List<model.mdlRecord>>() {
		}.getType();
		List<model.mdlRecord> recordList = gson.fromJson(rowArray, listType);

		if (recordList != null && recordList.size() > 0) {
		    model.mdlRecord mdlRecord = new model.mdlRecord();
		    mdlRecord = recordList.get(0);
		    if (mdlRecord.Record != null && mdlRecord.Record.size() > 0) {
			String k1Status = mdlRecord.Record.get(0);
			if (k1Status.equalsIgnoreCase("SUCCESS")) {
			    mObj.addProperty("is_k1", "Y");
			} else {
			    mObj.addProperty("is_k1", "N");
			}
		    } else {
			mObj.addProperty("is_k1", "N");
		    }

		} else {
		    mObj.addProperty("is_k1", "N");
		}
	    }
	    mdlErrorSchema.ErrorCode = "00";
	    mdlMessage.Indonesian = "Berhasil";
	    mdlMessage.English = mdlLog.LogStatus = "Success";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlGetCISResult.ErrorSchema = mdlErrorSchema;
	    mdlGetCISResult.OutputSchema = gson.fromJson(mObj, Object.class);
	    jsonOut = gson.toJson(mdlGetCISResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
	} catch (Exception ex) {
	    mdlErrorSchema.ErrorCode = "04";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlGetCISResult.ErrorSchema = mdlErrorSchema;
	    mdlLog.ErrorMessage = ex.toString();
	    jsonOut = gson.toJson(mdlGetCISResult);
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("FAILED = ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut + ", Exception : "
		    + ex.toString(), ex);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}
	LogAdapter.InsertLog(mdlLog);
	return jsonOut;
    }

}
