package model;

import com.google.gson.annotations.SerializedName;

public class mdlParameter {
    @SerializedName(value = "key", alternate= "Key")
    public String key;
    @SerializedName(value = "value", alternate= "Value")
    public String value;
}