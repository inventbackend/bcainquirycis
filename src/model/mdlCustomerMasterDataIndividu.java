package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerMasterDataIndividu {
    @SerializedName(value="religion", alternate="Religion")
    public String Religion;
    @SerializedName(value="id_number", alternate="IdNumber")
    public String IdNumber;
    @SerializedName(value="id_expired_date", alternate="IdExpiredDate")
    public String IdExpiredDate;
    @SerializedName(value="other_identity", alternate="OtherIdentity")
    public model.mdlOtherIdentity OtherIdentity;
    @SerializedName(value="birth_place", alternate="BirthPlace")
    public String BirthPlace;
    @SerializedName(value="sex", alternate="Sex")
    public String Sex;
    @SerializedName(value="segmentation", alternate="Segmentation")
    public String Segmentation;
    @SerializedName(value="reference", alternate="Reference")
    public String Reference;
    @SerializedName(value="corporate_group", alternate="CorporateGroup")
    public String CorporateGroup;
    @SerializedName(value="cis_status", alternate="CISStatus")
    public String CISStatus;
    @SerializedName(value="branch_no", alternate="BranchNo")
    public String BranchNo;
    @SerializedName(value="branch_name", alternate="BranchName")
    public String BranchName;
    @SerializedName(value="citizenship", alternate="Citizenship")
    public String Citizenship;
    @SerializedName(value="tax_no", alternate="TaxNo")
    public String TaxNo;
    @SerializedName(value="tax_indicator", alternate="TaxIndicator")
    public String TaxIndicator;
    @SerializedName(value="mail_code", alternate="MailCode")
    public String MailCode;
    @SerializedName(value="solicit", alternate="Solicit")
    public String Solicit;
    @SerializedName(value="provide_data_permission", alternate="ProvideDataPermission")
    public String ProvideDataPermission;
    @SerializedName(value="sms_advertisement", alternate="SMSAdvertisement")
    public String SMSAdvertisement;
    @SerializedName(value="email_advertisement", alternate="EmailAdvertisement")
    public String EmailAdvertisement;
    @SerializedName(value="telephone_advertisement", alternate="TelephoneAdvertisement")
    public String TelephoneAdvertisement;
    @SerializedName(value="primary_officer", alternate="PrimaryOfficer")
    public String PrimaryOfficer;
    @SerializedName(value="citizenship_code", alternate="CitizenshipCode")
    public String CitizenshipCode;
    @SerializedName(value="fatca_form_flag", alternate="FATCAFormFlag")
    public String FATCAFormFlag;
    @SerializedName(value="aeoi_form_flag", alternate="AEOIFormFlag")
    public String AEOIFormFlag;
    @SerializedName(value="fatca_assessable", alternate="FATCAAssessable")
    public String FATCAAssessable;
    @SerializedName(value="aeoi_assessable", alternate="AEOIAssessable")
    public String AEOIAssessable;
    @SerializedName(value="open_date", alternate="OpenDate")
    public String OpenDate;
    @SerializedName(value="close_date", alternate="CloseDate")
    public String CloseDate;
    @SerializedName(value="since_date", alternate="SinceDate")
    public String SinceDate;
    @SerializedName(value="fatca_tax_no", alternate="FATCATaxNo")
    public String FATCATaxNo;
    @SerializedName(value="customer_code", alternate="CustomerCode")
    public List<String> CustomerCode;
    @SerializedName(value="economic_sector", alternate="EconomicSector")
    public String EconomicSector;
    @SerializedName(value="collectibility", alternate="Collectibility")
    public String Collectibility;
    @SerializedName(value="credit_category", alternate="CreditCategory")
    public String CreditCategory;
}
