package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlInput {
    @SerializedName(value="store_procedure_name", alternate = "StoreProcedureName")
    public String StoreProcedureName;
    @SerializedName(value="parameter", alternate = "Parameter")
    public List<model.mdlParameter> Parameter;
}
