package model;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerDemographicInformation {
    @SerializedName(value="income", alternate="Income")
    public String Income;
    @SerializedName(value="birth_date", alternate="BirthDate")
    public String BirthDate;
    @SerializedName(value="birth_country_code", alternate="BirthCountryCode")
    public String BirthCountryCode;
    @SerializedName(value="house_ownership", alternate="HouseOwnership")
    public String HouseOwnership;
    @SerializedName(value="marital_status", alternate="MaritalStatus")
    public String MaritalStatus;
    @SerializedName(value="education", alternate="Education")
    public String Education;
    @SerializedName(value="occupation", alternate="Occupation")
    public String Occupation;
    @SerializedName(value="mothers_name", alternate="MothersName")
    public String MothersName;
    @SerializedName(value="death_date", alternate="DeathDate")
    public String DeathDate;
}