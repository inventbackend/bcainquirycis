package model;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerRemark {
    @SerializedName(value="remark", alternate="Remark")
    public String Remark;
    @SerializedName(value="remark_effective_date", alternate="RemarkEffectiveDate")
    public String RemarkEffectiveDate;
    @SerializedName(value="remark_expired_date", alternate="RemarkExpiredDate")
    public String RemarkExpiredDate;
    @SerializedName(value="alert", alternate="Alert")
    public String Alert;
}