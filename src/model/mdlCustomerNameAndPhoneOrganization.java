package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerNameAndPhoneOrganization {
    @SerializedName(value="full_name", alternate="FullName")
    public String FullName;
    @SerializedName(value="telex_number", alternate="TelexNumber")
    public List<String> TelexNumber;
    @SerializedName(value="office_phone", alternate="OfficePhone")
    public model.mdlHomePhone OfficePhone;
    @SerializedName(value="fax_phone", alternate="FaxPhone")
    public model.mdlHomePhone FaxPhone;
}
