package model;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerNameAndPhoneIndividu {
    @SerializedName(value="title", alternate="Title")
    public String Title;
    @SerializedName(value="full_name", alternate="FullName")
    public String FullName;
    @SerializedName(value="telex_number", alternate="TelexNumber")
    public String TelexNumber;
    @SerializedName(value="home_phone", alternate="HomePhone")
    public model.mdlHomePhone HomePhone;
    @SerializedName(value="office_phone", alternate="OfficePhone")
    public model.mdlHomePhone OfficePhone;
    @SerializedName(value="fax_phone", alternate="FaxPhone")
    public model.mdlHomePhone FaxPhone;
}
