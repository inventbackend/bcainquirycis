package model;

import com.google.gson.annotations.SerializedName;

public class mdlHandPhone {
    @SerializedName(value="country_code", alternate="CountryCode")
    public String CountryCode;
    @SerializedName(value="phone_number", alternate="PhoneNumber")
    public String PhoneNumber;
}