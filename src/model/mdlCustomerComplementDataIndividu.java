package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerComplementDataIndividu {
    @SerializedName(value="kitas", alternate="Kitas")
    public String Kitas;
    @SerializedName(value="kitas_expired_date", alternate="KitasExpiredDate")
    public String KitasExpiredDate;
    @SerializedName(value="income", alternate="Income")
    public String Income;
    @SerializedName(value="account_purpose", alternate="AccountPurpose")
    public String AccountPurpose;
    @SerializedName(value="company_name", alternate="CompanyName")
    public String CompanyName;
    @SerializedName(value="position", alternate="Position")
    public String Position;
    @SerializedName(value="business_sector", alternate="BusinessSector")
    public String BusinessSector;
    @SerializedName(value="contact_information", alternate="ContactInformation")
    public model.mdlContactInformation ContactInformation;
    @SerializedName(value="company_address", alternate="CompanyAddress")
    public List<String> CompanyAddress;
}