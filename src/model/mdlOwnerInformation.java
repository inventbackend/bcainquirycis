package model;

import com.google.gson.annotations.SerializedName;

public class mdlOwnerInformation {
    @SerializedName(value="name", alternate="Name")
    public String Name;
    @SerializedName(value="identification_number", alternate="IdentificationNumber")
    public String IdentificationNumber;
    @SerializedName(value="tax_number", alternate="TaxNumber")
    public String TaxNumber;
    @SerializedName(value="country_of_origin", alternate="CountryOfOrigin")
    public String CountryOfOrigin;
}
