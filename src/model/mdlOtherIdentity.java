package model;

import com.google.gson.annotations.SerializedName;

public class mdlOtherIdentity {
    @SerializedName(value="id_type", alternate="IdType")
    public String IdType;
    @SerializedName(value="id_number", alternate="IdNumber")
    public String IdNumber;
}