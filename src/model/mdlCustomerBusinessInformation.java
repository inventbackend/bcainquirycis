package model;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerBusinessInformation {
    @SerializedName(value="business_type", alternate="BusinessType")
    public String BusinessType;
    @SerializedName(value="business_since_date", alternate="BusinessSinceDate")
    public String BusinessSinceDate;
    @SerializedName(value="corporate_income", alternate="CorporateIncome")
    public String CorporateIncome;
    @SerializedName(value="corporate_wealthy", alternate="CorporateWealthy")
    public String CorporateWealthy;
    @SerializedName(value="corporate_bca_prime_code", alternate="CorporateBCAPrimeCode")
    public String CorporateBCAPrimeCode;
}
