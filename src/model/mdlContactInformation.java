package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlContactInformation {
    @SerializedName(value="district", alternate="District")
    public String District;
    @SerializedName(value="sub_district", alternate="SubDistrict")
    public String SubDistrict;
    @SerializedName(value="street", alternate="Street")
    public String Street;
    @SerializedName(value="city", alternate="City")
    public String City;
    @SerializedName(value="country", alternate="Country")
    public String Country;
    @SerializedName(value="province", alternate="Province")
    public String Province;
    @SerializedName(value="building", alternate="Building")
    public String Building;
    @SerializedName(value="rw", alternate="Rw")
    public String Rw;
    @SerializedName(value="rt", alternate="Rt")
    public String Rt;
    @SerializedName(value="zip_code", alternate="ZipCode")
    public String ZipCode;
    @SerializedName(value="hand_phone", alternate="HandPhone")
    public List<model.mdlHandPhone> HandPhone;
    @SerializedName(value="email_address", alternate="EmailAddress")
    public String EmailAddress;

}