package model;

import com.google.gson.annotations.SerializedName;

public class mdlCISIndividu {
    @SerializedName(value="cis_customer_number", alternate="CISCustomerNumber")
    public String CISCustomerNumber;
    
    @SerializedName(value="cis_customer_type", alternate="CISCustomerType")
    public String CISCustomerType;
    
    @SerializedName(value="customer_master_data", alternate="CustomerMasterData")
    public model.mdlCustomerMasterDataIndividu CustomerMasterData;
    
    @SerializedName(value="customer_name_and_phone", alternate="CustomerNameAndPhone")
    public model.mdlCustomerNameAndPhoneIndividu CustomerNameAndPhone;
    
    @SerializedName(value="customer_address", alternate="CustomerAddress")
    public model.mdlCustomerAddress CustomerAddress;
    
    @SerializedName(value="customer_demographic_information", alternate="CustomerDemographicInformation")
    public model.mdlCustomerDemographicInformation CustomerDemographicInformation;
    
    @SerializedName(value="customer_complement_data", alternate="CustomerComplementData")
    public model.mdlCustomerComplementDataIndividu CustomerComplementData;
    
    @SerializedName(value="customer_remark", alternate="CustomerRemark")
    public model.mdlCustomerRemark CustomerRemark;
    
    @SerializedName(value="cis_last_update", alternate="CISLastUpdate")
    public model.mdlCISLastUpdate CISLastUpdate;
    
    @SerializedName(value="is_k1", alternate="IsK1")
    public String IsK1;
}