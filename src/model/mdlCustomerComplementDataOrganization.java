package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlCustomerComplementDataOrganization {
    @SerializedName(value="income", alternate="Income")
    public String Income;
    @SerializedName(value="account_purpose", alternate="AccountPurpose")
    public String AccountPurpose;
    @SerializedName(value="business_sector", alternate="BusinessSector")
    public String BusinessSector;
    @SerializedName(value="contact_information", alternate="ContactInformation")
    public model.mdlContactInformation ContactInformation;
    @SerializedName(value="owner_information", alternate="OwnerInformation")
    public List<model.mdlOwnerInformation> OwnerInformation;
    @SerializedName(value="management_information", alternate="ManagementInformation")
    public List<model.mdlOwnerInformation> ManagementInformation;
}
