package model;

import com.google.gson.annotations.SerializedName;

public class mdlCISLastUpdate {
    @SerializedName(value="update_date", alternate="UpdateDate")
    public String UpdateDate;
    @SerializedName(value="officer_user_id", alternate="OfficerUserID")
    public String OfficerUserID;
}