package model;

import com.google.gson.annotations.SerializedName;

public class mdlHomePhone {
    @SerializedName(value="country_code", alternate="CountryCode")
    public String CountryCode;
    @SerializedName(value="area_code", alternate="AreaCode")
    public String AreaCode;
    @SerializedName(value="phone_number", alternate="PhoneNumber")
    public String PhoneNumber;
}
