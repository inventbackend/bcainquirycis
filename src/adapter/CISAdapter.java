package adapter;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class CISAdapter {
    final static Logger logger = LogManager.getLogger(CISAdapter.class);

    static Client client = Client.create();
    model.mdlLog mdlLog = new model.mdlLog();

    public static model.mdlAPIResult GetCISData(String cis, String K1Flag, String functionCode, String serialNumber, String wsid) {
	long startTime = System.currentTimeMillis();
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getCIS";
	mdlLog.SystemFunction = "GetCISData";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = serialNumber;
	mdlLog.WSID = wsid;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	String jsonIn, jsonOut = "";

	Gson gson = new Gson();
	String urlAPI = "/cis/db2/" + cis + "?FunctionCode=" + functionCode;

	Object[] params = new Object[] { cis, K1Flag, functionCode, serialNumber, wsid };
	jsonIn = MessageFormat.format("CIS : {0}, K1Flag : {1}, FunctionCode : {2}, SerialNumber : {3}, WSID : {4}", params);

	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("ClientID", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS. ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, function : GetCISData, jsonIn:" + jsonIn + ", jsonOut : " + jsonOut);

	    mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (mdlAPIResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.error("FAILED. ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, function : GetCISData, jsonIn:" + jsonIn + ", Exception : " + ex.toString(), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }

    public static model.mdlAPIResult GetK1Flag(String cis, String serialNumber, String wsid) {
	long startTime = System.currentTimeMillis();
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getCIS";
	mdlLog.SystemFunction = "GetK1Flag";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = serialNumber;
	mdlLog.WSID = wsid;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	Gson gson = new Gson();
	String urlAPI = "/eaidb/v2/store-procedure";
	String jsonIn = "";
	String jsonOut = "";

	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // create json input to hit stored procedure k1 customer
	    model.mdlStoredProcedure mdlStoredProcedure = new model.mdlStoredProcedure();
	    model.mdlInput mdlInput = new model.mdlInput();
	    model.mdlParameter mdlParameter = new model.mdlParameter();
	    List<model.mdlParameter> parameterList = new ArrayList<model.mdlParameter>();
	    mdlInput.StoreProcedureName = "MDM.PKG_MDM_EXT.SP_CHECK_K1_CUSTOMER";
	    mdlParameter.key = "cis_number";
	    mdlParameter.value = cis;
	    parameterList.add(mdlParameter);
	    mdlParameter = new model.mdlParameter();
	    mdlParameter.key = "application_key";
	    mdlParameter.value = "2";
	    parameterList.add(mdlParameter);
	    mdlInput.Parameter = parameterList;
	    mdlStoredProcedure.Input = mdlInput;

	    jsonIn = gson.toJson(mdlStoredProcedure);
	    jsonIn.replaceAll("\\s+", ""); // canonicalize JSON (remove all whitespace like \r, \n, \t and space)
	    
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("client-id", decryptedClientID).post(ClientResponse.class, jsonIn);
	    jsonOut = response.getEntity(String.class);
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS. ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, function : GetK1Flag, jsonIn:" + jsonIn + ", jsonOut : " + jsonOut);

	    mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    if (mdlAPIResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    logger.error("FAILED. ResponseTime : " + elapsedTime + ", API : GetCIS, method : GET, function : GetK1Flag, jsonIn:" + jsonIn + ", Exception : " + ex.toString(), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }

    public static model.mdlAPIResult getCISByKTP(String ktpNumber, String WSID, String SerialNumber) {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;
	model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.ApiFunction = "getCIS";
	mdlLog.SystemFunction = "getAccountListByKTP";
	mdlLog.LogSource = "Middleware";
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.WSID = WSID;
	mdlLog.LogStatus = "Success";
	mdlLog.ErrorMessage = "";

	String jsonIn = "type:ktp, id:" + ktpNumber + ", WSID:" + WSID + ", SerialNumber:" + SerialNumber;
	String jsonOut = "";
	Gson gson = new Gson();

	String urlAPI = "/cis/search/db2?SearchBy=IdNumber&IdNumber=" + ktpNumber;
	try {
	    // Get the base naming context from web.xml
	    Context context = (Context) new InitialContext().lookup("java:comp/env");

	    // Get a single value from web.xml
	    String MiddlewareIpAddress = (String) context.lookup("param_ipmiddleware");
	    String urlFinal = MiddlewareIpAddress + urlAPI;
	    String keyAPI = (String) context.lookup("param_keyAPI");
	    String ClientID = (String) context.lookup("param_clientid");
	    String decryptedClientID = EncryptAdapter.decrypt(ClientID, keyAPI);

	    // Client client = Client.create();
	    WebResource webResource = client.resource(urlFinal);
	    ClientResponse response = webResource.type("application/json").header("ClientID", decryptedClientID).get(ClientResponse.class);
	    jsonOut = response.getEntity(String.class);

	    stopTime = System.currentTimeMillis();

	    mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
	    elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS. API : GetCIS, method : GET, Time :" + elapsedTime + " ms, function : getCISByKTP, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
	    
	    if (mdlAPIResult == null) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "API output is null";
	    } else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")) {
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	    }
	} catch (Exception ex) {
	    mdlAPIResult = null;
	    mdlLog.LogStatus = "Failed";
	    logger.error("FAILED. API : GetCIS, method : GET, Time:" + elapsedTime + " ms, function : getCISByKTP, jsonIn:" + jsonIn + ", Exception : " + ex.toString(), ex);
	    mdlLog.ErrorMessage = ex.toString();
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAPIResult;
    }
}
