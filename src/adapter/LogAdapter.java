package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import model.Globals;

public class LogAdapter {
    final static Logger logger = LogManager.getLogger(LogAdapter.class);

    public static void InsertLog(model.mdlLog logModel) {
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	try {
	    String dateNowString = ConvertDateTimeHelper.GetDateTimeNowCustomFormat("yyyyMMddHHmmssSSS");
	    StringBuilder sb = new StringBuilder();
	    sb.append("API-").append(dateNowString).append("-").append(logModel.WSID);
	    String newLogID = sb.toString();

	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "INSERT INTO APILog (LogID, LogDate, LogSource, SerialNumber, WSID, APIFunction, SystemFunction, LogStatus, ErrorMessage)"
		    + "VALUES (?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),?,?,?,?,?,?,?)";

	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    String dateNow = LocalDateTime.now().toString().replace("T", " ");
	    pstm.setString(1, newLogID);
	    pstm.setString(2, dateNow);
	    pstm.setString(3, logModel.LogSource);
	    pstm.setString(4, logModel.SerialNumber);
	    pstm.setString(5, logModel.WSID);
	    pstm.setString(6, logModel.ApiFunction);
	    pstm.setString(7, logModel.SystemFunction);
	    pstm.setString(8, logModel.LogStatus);
	    pstm.setString(9, logModel.ErrorMessage);

	    // execute query
	    jrs = pstm.executeQuery();
	} catch (Exception ex) {
	    logger.error("FAILED. API : GetCIS, Function : InsertLog, Exception : " + ex.toString(), ex);
	    System.out.print(ex.toString());
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		logger.error("FAILED. API : GetCIS, Function : Close Connection InsertLog, Exception : " + e.toString(), e);
		Globals.gReturn_Status = "Insert log failed";
	    }
	}
	return;
    }
}
